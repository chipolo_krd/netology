Необходимо выбрать все данные из таблицы books (если используете сервер Нетологии, 
то база данных global, если нет, то дамп базы данных находится в файле dump.txt)     --сделал
и вывести их на странице в виде таблицы.





Расширенное задание:

Добавить возможность фильтровать данные по трем параметрам: ISBN, name, author.
 Для ввода данных для фильтрации следует использовать текстовые поля. Фильтр        ---сделал
должен работать по принципу поиска введенной подстроки в любом месте строки
 (как мы разбирали во время лекции).



Задание для самых отчаянных:

Фильтры должны суммироваться, т.е. если пользователь ввел фильтр по ISBN, 
который возвращает 3 записи, и при этом ввел фильтр по имени автора, который 
возвращает 2 записи, то в результате будут выведены записи, которые соответсвуют   --сделал
 фильтру по ISBN и по имени автора. При этом введенные значения фильтров должны 
сохраняться в полях для ввода после вывода страницы с результатами.