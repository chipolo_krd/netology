<?php

/*
 *
  контроллер создания записи
  выдает форму добавления записи с полями «имя» и «сообщение»,
  форма отправляется POST-запросом,
  если форма отправлена, и все поля заполнены, создает запись и переадресует на
 *  страницу списка записей,
  если форма отправлена, и не все поля заполнены, выдает сообщение об ошибках и
 *  форму в том состоянии как она была отправлена.
 */
require_once __DIR__ . '/config.php';
require_once '/controller/controller.php';
require_once __DIR__ . '/model/guestbook.php';
require_once __DIR__ . '/vendor/autoload.php';


$guestbook = new Guestbook($dbuser, $dbpass, $dbhost, $dbname);
$controller = new Controller();

If ( isset($_POST['name'])and isset($_POST['message']) )
    {
    $controller->getPost('name');
    $post['name'] = $controller->name;
    $controller->getPost('message');
    $post['message'] = $controller->message;

    If ( $post['name'] !== "" and $post['message'] !== "" )
        {
        $guestbook->create($post);
        $controller->redirect();
        }
    else
        {
        $controller->getParams();
        $params = $controller->params;
        $controller->display("view.phtml", $params);
        echo $controller->template;
        }
    }
else
    {
    $params = $controller->params;
    $controller->display("view.phtml", $params);
    echo $controller->template;
    }
