<?php

class Controller {

    public $post;
    public $params;
    public $template;
    public $name;
    public $message;

    function __construct() {
        $this->params = array(
            'name' => "",
            'message' => "",
            'error' => "ok",
        );
    }

    function getPost($var) {
        $cleanvar = htmlspecialchars(trim($_POST[$var]));
        if ( $var == 'name' )
            {
            $this->name = $cleanvar;
            }
        if ( $var == 'message' )
            {
            $this->message = $cleanvar;
            }
    }

    function display($page, $params) {

        require_once './vendor/autoload.php';
        Twig_Autoloader::register();
        try {
            $loader = new Twig_Loader_Filesystem('view');
            $twig = new Twig_Environment($loader);
            $template = $twig->loadTemplate($page);
            $this->template = $template->render($params);
            
        }
        catch (Exception $e) {

            die('ERROR: ' . $e->getMessage());
        }
    }

    function getParams() {
        echo $this->message;
        echo $this->name;
        If ( $this->name == "" )
            {
            $this->params = array(
                'message' => $this->message,
                'error_info' => "поле не заполнено",
                'error' => "name",
            );
            
            }
        If ( $this->message == "" )
            {
            $this->params = array(
                'name' => $this->name,
                'error_info' => "поле не заполнено",
                'error' => "message",
            );
            
            }
    }
function redirect($page = "index.php")
{
    $host = htmlspecialchars(trim($_SERVER['HTTP_HOST']));
    $dir = dirname(htmlspecialchars(trim($_SERVER['PHP_SELF'])));
    $url = "http://" . $host . $dir . "/" . $page;
    header("Location: $url");
}
}
