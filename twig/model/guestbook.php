<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Guestbook
{

    public $dbuser;
    public $dbpass;
    public $dbhost;
    public $dbname;
//    public $tablename;
    public $pdo;
    public $answerDB;
    public $id;

    public function __construct($dbuser, $dbpass, $dbhost, $dbname)
    {

	$this->dbuser = $dbuser;
	$this->dbpass = $dbpass;
	$this->dbhost = $dbhost;
	$this->dbname = $dbname;
	$pdo = new PDO("mysql:host=$dbhost;dbname=$dbname", "$dbuser", "$dbpass");
	$pdo->exec('SET NAMES utf-8');
	$pdo->exec("SET CHARACTER SET 'utf8';");
	$pdo->exec("SET SESSION collation_connection = 'utf8_general_ci';");
	$this->pdo = $pdo;
    }

    public function russianDate($date)
    {
	$day = date("d", strtotime($date));
	$month = date("n", strtotime($date));
	switch ($month)
	{
	    case 1: $month = 'Января';
		break;
	    case 2: $month = 'Февраля';
		break;
	    case 3: $month = 'Марта';
		break;
	    case 4: $month = 'Апреля';
		break;
	    case 5: $month = 'Мая';
		break;
	    case 6: $month = 'Июня';
		break;
	    case 7: $month = 'Июля';
		break;
	    case 8: $month = 'Августа';
		break;
	    case 9: $month = 'Сентября';
		break;
	    case 10: $month = 'Октября';
		break;
	    case 11: $month = 'Ноября';
		break;
	    case 12: $month = 'Декабря';
		break;
	}
	$year = date("Y", strtotime($date));
	$hour = date("H", strtotime($date));
	$minute = date("i", strtotime($date));
	$date = $day . " " . $month . " " . $year . " в " . $hour . ":" . $minute;
	return ($date);
    }

    public function last()
    {
	$sql = "SELECT * FROM `guestbook` ORDER BY `crdate` DESC LIMIT 20";

	$pdo = $this->pdo;

	foreach ($pdo->query($sql) as $v)
	{
	    $this->answerDB['list'][] = [
		'name' => $v['name'],
		'message' => $v['message'],
		'crdate' => $this->russianDate($v['crdate'])
	    ];
	}
    }

    public function create($post)
    {
//	$date = date('Y-m-d H:i:s');
	$name = $post['name'];
	$message = $post['message'];
	$sql = "INSERT INTO `guestbook` (`crdate`, `name`, `message`)VALUES (now(), '$name', '$message');";
	$pdo = $this->pdo;
	$pdo->exec($sql);
	$sql = "SELECT * FROM `guestbook` WHERE `crdate` = '$date';";
	foreach ($pdo->query($sql) as $row)
	{
	    $this->id = $row['id'];
	}
    }

}
