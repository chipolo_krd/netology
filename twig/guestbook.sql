SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';
DROP TABLE IF EXISTS `guestbook`;
CREATE TABLE `guestbook` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`crdate` DATETIME NOT NULL,
	`name` varchar(255) NOT NULL,
	`message` TEXT NOT NULL,
 PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;
