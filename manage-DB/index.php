<?php

function sqlQuery($sql) {
    $host = "localhost";
    $dbname = "tsibulnik";
    $user_root = "tsibulnik";
    $sql_pass = 'neto0412';

    /* Подключаемся */
    $pdo = new PDO("mysql:host=$host;dbname=$dbname", "$user_root", "$sql_pass");
    $pdo->exec('SET NAMES utf-8');
    $pdo->exec("SET CHARACTER SET 'utf8';");
    $pdo->exec("SET SESSION collation_connection = 'utf8_general_ci';");
    foreach ( $pdo->query($sql) as $row )
        {
        $tables[] = $row["Tables_in_" . $dbname];
        }
    return($tables);
}

function getDescribe($sql) {
    $host = "localhost";
    $dbname = "tsibulnik";
    $user_root = "tsibulnik";
    $sql_pass = 'neto0412';

    /* Подключаемся */
    $pdo = new PDO("mysql:host=$host;dbname=$dbname", "$user_root", "$sql_pass");
    $pdo->exec('SET NAMES utf-8');
    $pdo->exec("SET CHARACTER SET 'utf8';");
    $pdo->exec("SET SESSION collation_connection = 'utf8_general_ci';");
    foreach ( $pdo->query($sql) as $row )
        {
        $tables[] = [
            'Field' => $row['Field'],
            'Type' => $row['Type'], ];
        }
    return($tables);
}
?>


<style>
    table { 
        border-spacing: 0;
        border-collapse: collapse;
    }

    table td, table th {
        border: 1px solid #ccc;
        padding: 5px;
    }

    table th {
        background: #eee;
    }

    form {
        margin: 0;
    }
</style>

<h1>Анализируем таблицы в базе данных:</h1>
<div style="margin-bottom: 20px;">
    <form method="GET">
        <label for="table">Выберите таблицу:</label>
        <select name="table">
            <?php
            $sql = "show tables;";
            $tables = sqlQuery($sql);
            foreach ( $tables as $value )
                {
                echo '<option value="' . $value . '">' . $value . '</option>';
                }
            ?>

        </select>
        <input type="submit" value="Показать таблицу" />
    </form>
</div>
<div style="clear: both"></div>
<?php
if ( isset($_GET['table']) )
    {
    $table = htmlspecialchars(trim($_GET['table']));
    $sql = "DESCRIBE $table;";
    $tables = getDescribe($sql);

    echo '<table>';
    echo '<tr><th>Поле</th><th>Тип</th></tr>';
    foreach ( $tables as $value )
        {
        echo '<tr>';
        $field = $value['Field'];
        echo "<td>$field</td>";
        $type = $value['Type'];
        echo "<td>$type</td>";
        echo '</tr>';
        }
    echo '</table>';
    unset($_GET);
    }
?>
