<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Список человеков</title>
    <style>
        body{margin: 50px auto;}
        h1{text-align: center;}
        table{margin: 10px auto; width: 80%}
        th{padding: 20px;text-align: center; width: 33%;text-transform: uppercase;}
        td{padding: 15px;text-align: center; width: 33%;}
    </style>
    </head>
    <body>
        <h1>Список человеков</h1>
        <hr>
        <table>
            <tr>

<?php
$row = 1;
$csv = "data.csv";
if (($handle = fopen($csv, "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
        $num = count($data);
   
            echo "<tr>";
        if ($row == 1){
                     
            for ($c=0; $c < $num; $c++) {?>
                <th><?=$data[$c]?></th>
            <?php 
            }
        }else{
        for ($c=0; $c < $num; $c++) {?>
            <td><?=$data[$c]?></td>
        <?php }}
        $row++;
        }
    fclose($handle);
}
?>
            </tr>
        </table>
        <hr>
    </body>
</html>

