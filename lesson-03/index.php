<?php

$cities = array(
    "Краснодарский край" => array("Краснодар", "Сочи", "Горячий ключ", "Голубая бухта"),
    "Приморский край" => array("Большой Камень"),
    "Псковская область" => array("Великие Луки"),
    "Новгородская область" => array("Великий Новгород"),
    "Вологодская область" => array("Великий Устюг")
);



foreach ($cities as $k => $v)
{
    $country = $k; // страна - ключ
    foreach ($v as $city)
    {
	//echo "$city <br>";// в $city будут попадать города
	$city = trim($city);
	$w_cities = explode(" ", $city);
	if (count($w_cities) === 2)
	{
	    $r_cities[] = $w_cities[1];
	    $l_cities[] = $w_cities[0] . $k;
	    $new_reg[$w_cities[0] . $k] = array($w_cities[0], $k);

	    shuffle($r_cities);
	    shuffle($l_cities);
	}
    }
}
foreach ($l_cities as $k => $v)
{
    $new_new[$new_reg[$v][1]][] = $new_reg[$v][0] . ' ' . $r_cities[$k];
}

foreach ($new_new as $k => $v)
{
    echo "Регион: $k\n";
    foreach ($v as $ki => $vi)
    {
	echo " Город: $vi\n";
    }
}
?>