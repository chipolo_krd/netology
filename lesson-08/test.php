<?php 
session_start();

$user_name = $_SESSION['login'];

 if (empty($_GET['id'])){
     $host= $_SERVER['HTTP_HOST'];
$dir = dirname($_SERVER['PHP_SELF']);
$url = "http://".$host.$dir.'/list.php';

header("Location: $url");
 }
 
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
	<meta name="viewport" content="width=device-width">
        <title>Список файлов</title>
        <style>
            div{margin: 20px auto;text-align: center;}
        </style>
    </head>
    <body>
        
<?php
function create_img($user_name, $title_quiz, $copyright) {
    $backImage = imagecreatefrompng(__DIR__."/img/cert.png");
    $textcolor = imagecolorallocate($backImage, 255,255, 255);
    $fontFile = realpath(__DIR__.'/fonts/exo-2-bold.ttf');
  $pathimage = __DIR__."/img/cert_".$user_name."."."png";
    imagettftext($backImage, 30, 0, 340, 45, $textcolor, $fontFile, "Поздравляем!");
    imagettftext($backImage, 24, 0, 29, 105, $textcolor, $fontFile, $user_name.", вы сдали тест:");
    imagettftext($backImage, 28, 0, 340, 185, $textcolor, $fontFile, $title_quiz);
    imagettftext($backImage, 10, 0, 629, 635, $textcolor, $fontFile, "С уважением, администрация сайта");
    imagettftext($backImage, 10, 0, 629, 665, $textcolor, $fontFile, $copyright);
    
    imagepng($backImage, $pathimage);
    imagedestroy($backImage);
}

/* 

 файл test.php, который принимает в качестве
GET параметра номер теста, и отображает форму теста.
Если форма отправлена, проверяет и показывает результат.
 */

if(isset($_GET['id'])==TRUE && isset($_GET['user_answer'])==FALSE){
    $current_id = $_GET['id'];
$json = __DIR__."/JSON/data.json";
$decode = file_get_contents("$json");
$arr = json_decode($decode);
for($i=0;$i<count($arr);$i++){
    $id = $arr[$i]->id;
    $title_quiz = $arr[$i]->title_quiz;
    $question = $arr[$i]->question;
    $answer = $arr[$i]->answer;
    
     if($current_id==$id){
        echo "<h1>".$title_quiz."</H1><hr>".PHP_EOL;
        echo "<i>".$question."</i>".PHP_EOL;
        echo "<br>";
        echo "<br>";
        echo "<form action='test.php' method='get'>";
        echo "<input type='text' placeholder='Каков Ваш ответ?' name='user_answer'>".PHP_EOL;
        echo "<input type='hidden'  name='id' value='".$id."'>".PHP_EOL;
        echo "<input type='submit' value='Мой ответ'>".PHP_EOL;
        echo "</form>".PHP_EOL;
        break;
       }
                     
      if($current_id!==$i && $i==count($arr)-1) { 
      header ("HTTP/1.0 404 Not Found");
        echo "Not found";
        exit;
      
      }
    }
       
  
    }

     
    if(isset($_GET['user_answer'])==TRUE && isset($_GET['id'])==TRUE)
    {
         
        $json = __DIR__."/JSON/data.json";
        $decode = file_get_contents("$json");
        $arr = json_decode($decode);
        $current_id = $_GET['id'];
        $user_answer = $_GET['user_answer'];
        
        
              
        for($i=0;$i<count($arr);$i++)
        {
            $id = $arr[$i]->id;
            $answer = $arr[$i]->answer;
            $copyright=$_SERVER['SERVER_NAME'];
            $title_quiz = $arr[$i]->title_quiz;
            
            if($id==$current_id && $answer==$user_answer)
            {
                echo "<div><a href='list.php'>Выбрать другой тест</a></div>".PHP_EOL;
                //header('Content-Type: image/png');
                if(is_file(__DIR__."/img/cert_".$user_name."."."png")){
                    unlink(__DIR__."/img/cert_".$user_name."."."png");
                }
                create_img($user_name, $title_quiz, $copyright);?>
        <div><img src="img/cert_<?=$user_name?>.png"></div>
                
                <?php break;
            }
           
            if($id==$current_id && $answer!==$user_answer)
            {
                echo "<h1>Тест не сдан</H1><hr>".PHP_EOL;
                echo "<a href='test.php?id=".$id."'>Начать тест заново</a>".PHP_EOL;
                break;

            }
        }
     }
 
?>
    </body>
</html>
    

    