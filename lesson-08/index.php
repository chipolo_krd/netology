<?php session_start();?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
	<meta name="viewport" content="width=device-width">
        <title>Центр онлайн тестирования</title>
        <style>
        body{margin: 50px auto;}
        .clearfix:after {content: "";display: table;clear: both;}
        .error{color: darkred; width: 100px}
        .form{width: 90%;margin: 50px auto;}
        .login, .reg, .guest{float: left;  margin: 22px;width: 30%;min-width: 120px;}
        .reg{margin-right:  0px;}
        input{float: left; margin: 5px;width: 155px;}
        
        </style>
    </head>
    <body>
        <?php if(!empty($_SESSION['error'])):?>
        <b class="error"><?=$_SESSION['error']?></b>
        <?php unset($_SESSION['error']);?>
        <?php endif;?>
        <?php if(!empty($_SESSION['islogin'])):?>
           <?php 
           $host= $_SERVER['HTTP_HOST'];
$dir = dirname($_SERVER['PHP_SELF']);
$url = __DIR__.'/list.php';

           require_once $url;?>
        <?php else: ?>
                      
                <div class="form clearfix">
                    <div class="login">
                        <h2>Войти</h2>
                        <form action="auth.php" method="post" class="clearfix">
                            <input type="text" name="User[login]" placeholder="input name" required>
                            <input type="password" name="User[pass]" placeholder="input password" required>
                            <input type="submit" value="Войти">
                        </form>
                    </div>
                    <div class="reg">
                        <h2>Зарегистрироваться</h2>
                        <form action="reg.php" method="post" class="clearfix">
                            <input type="text" name="New_User[login]" placeholder="input name" required>
                            <input type="password" name="New_User[pass]" placeholder="input password" required>
                            <input type="submit" value="Зарегистрироваться">
                        </form>
                    </div>
                    <div class="guest">
                        <h2>Гостевой вход</h2>
                        <form action="list.php" method="post" class="clearfix">
                                <input type="text" name="guest_name" placeholder="input name" required>
                                <input type="submit" value="Гостевой вход">
                            </form>
                    </div>
                </div>
        <?php endif;?>
    </body>
</html>


