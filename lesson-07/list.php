<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
	<meta name="viewport" content="width=device-width">
        <title>Список тестов</title>
        <style>
        body{margin: 50px auto;}
        h1{text-align: center;}
        table{margin: 10px auto; width: 80%}
        th{padding: 20px;text-align: center; width: 33%;text-transform: uppercase;}
        td{padding: 15px;text-align: center; width: 33%;}
        </style>
    </head>
    <body>
        <h1>Список тестов</h1>
        <hr>
        <table>
            <tr>
                <th>Айди теста</th>
                <th>название теста</th>
                <th></th>
            </tr>
            
<?php

/* 
 *Формируем список тестов, желательно сделать вывод на страницу для выбора теста из списка
 */
$json = __DIR__."/JSON/data.json";
$decode = file_get_contents("$json");
$arr = json_decode($decode);


for($i=0;$i<count($arr);$i++){
    echo "<tr>".PHP_EOL;
    $id = $arr[$i]->id;
    $title_quiz = $arr[$i]->title_quiz;
    echo "<td>".$id."</td>".PHP_EOL;
    echo "<td>".$title_quiz."</td>".PHP_EOL;
    echo "<td><a href='test.php?id=".$id."'>Начать тест</a></td>".PHP_EOL;
    echo "</tr>".PHP_EOL;
    }

?>
        </table>
        <hr>
    </body>
</html>