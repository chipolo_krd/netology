<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include './connectdb.php';
session_start();
$user_name = $_SESSION['login'];
function check_sort($sort)
{
    if (isset($_GET['sort']))
    {
	if ($_GET['sort'] == $sort)
	{
	    echo "sort-active";
	}
    }
}

function sql_query($sql)
{
    $pdo = ConnectBD();
    foreach ($pdo->query($sql) as $row)
    {
	echo "<tr>" . PHP_EOL;
	echo "<td>" . $row['description'] . "</td>" . PHP_EOL;
	if ($row['is_done'] == 0)
	{
	    echo "<td style='color:red;'>Не выполнено</td>" . PHP_EOL;
	}
	if ($row['is_done'] == 1)
	{
	    echo "<td style='color:green;'>Выполнено</td>" . PHP_EOL;
	}
	echo "<td>" . $row['date_added'] . "</td>" . PHP_EOL;
	$_SESSION['id'] = $row['id'];
	$id = $row['id'];
	echo "<td><a class='btn' href='edit.php?id=$id'>Редактировать</a></td>" . PHP_EOL;
	echo "</tr>" . PHP_EOL;
    }
}
?>

<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
	<meta name="viewport" content="width=device-width">
        <title>Пупер-планер</title>
        <style>
	    h1 {
		text-align: center;
		margin-top: 50px;
	    }
            table { 
                border-spacing: 0;
                border-collapse: collapse;
                width: 90%;
		
                margin: 20px auto;
            }
            table td, table th {
                border: 1px solid #ccc;
                padding: 5px;
                width: 18%;
		text-align: center;
            }
            table th {
                background: #eee;
            }
	    .btn{
		display: block;
		text-decoration: none;
		text-transform: uppercase;
		color:#000;
		background-color: #eee;
		border: solid 1px #000;
		margin: 10px auto;
		max-width: 75%;
		border-radius: 10px;
		padding: 5px;
		font-size: 10px;
	    }
	    .btn-1{
		display: block;
		text-decoration: none;
		text-transform: uppercase;
		color:#000;
		background-color: #eee;
		border: solid 1px #000;
		margin: 10px auto;
		text-align: center;
		width: 50px;
		border-radius: 10px;
		padding: 10px;
		font-size: 10px;
	    }
	    .sort{
		display: block;
		float: left;
		text-decoration: none;
		color: #000;
		font-size: 35px;
	    }
	    .sort-active{
		color: blue;
	    }
	    hr{
		margin-bottom: 20px;
		margin-top: 20px;
	    }
	    
        </style>
    </head>
    <body>
	<a class="btn-1" href="logout.php">Выход</a>
        <h1>Список задач</h1>
	<hr>
	
        <table>
	    <thead>
		<tr>
		    <th><a class="sort <?php
			check_sort("up-task");
			?>" href="task.php?sort=up-task">ˆ</a><br><a class="sort <?php
			   check_sort("down-task");
			   ?>" href="task.php?sort=down-task">ˇ</a>Описание задачи</th>
		    <th><a class="sort <?php
			check_sort("up-status");
			?>" href="task.php?sort=up-status">ˆ</a><br><a class="sort <?php
			   check_sort("down-status");
			   ?>" href="task.php?sort=down-status">ˇ</a>Статус</th>
		    <th><a class="sort <?php
			check_sort("up-date");
			?>" href="task.php?sort=up-date">ˆ</a><br><a class="sort <?php
			   check_sort("down-date");
			   ?>" href="task.php?sort=down-date">ˇ</a>Дата изменения</th>
		    <th></th>
		</tr>
	    </thead>
            <tbody>
		<?php
		if (isset($_GET['sort']))
		{
		    $sort = $_GET['sort'];

		    switch ($sort)
		    {
			case "up-task":
			    $sql = "SELECT * FROM `tasks` ORDER BY `description` LIMIT 50";
			    break;
			case "down-task":
			    $sql = "SELECT * FROM `tasks` ORDER BY `description` DESC LIMIT 50";
			    break;
			case "up-status":
			    $sql = "SELECT * FROM `tasks` ORDER BY `is_done` LIMIT 50";
			    break;
			case "down-status":
			    $sql = "SELECT * FROM `tasks` ORDER BY `is_done` DESC LIMIT 50";
			    break;
			case "up-date":
			    $sql = "SELECT * FROM `tasks` ORDER BY `date_added`  LIMIT 50";
			    break;
			case "down-date":
			    $sql = "SELECT * FROM `tasks` ORDER BY `date_added` DESC LIMIT 50";
			    break;
		    }
		}
		else
		{
		    $sql = "select * from tasks";
		}
		sql_query($sql);
		?>
	    </tbody>
	</table>
    
	<a class="btn-1" href="edit.php?add=1">Добавить</a>
	<hr>
    </body>
</html>


