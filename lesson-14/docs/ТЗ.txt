Итак, задание к третьей лекции по базам данных.
 
За основу мы берем задание со второй лекции, наше TODO приложение, и дополняем его.
 
1. Добавляем регистрацию и авторизацию. --сделал
2. Добвляем возможность закреплять задачу за другим пользователем в системе.
3. Выводим для пользователя 2 списка задач: созданные им, и отдельно задачи закрепленные за ним другими пользователями.
 
Во время лекции я более подробно остановился на деталях домашнего задания, поскольку задание достаточно сложное. Рекомендую еще раз пересмотреть перед выполнением.
 
Пример выполнения можно найти по ссылке: http://university.netology.ru/u/vfilipov/l3/index.php
 
Дамп базы данных для выполнения задания можно найти по ссылке http://university.netology.ru/u/vfilipov/l3/dump_l3.txt
 
Всем удачи!​
